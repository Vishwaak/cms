FROM python:3.6

ENV PYTHONNUMBUFFERED 1

RUN mkdir /cms

WORKDIR /cms

ADD . /cms/

COPY requirements.txt /cms/

RUN pip install -r requirements.txt

COPY . /cms/

EXPOSE 23400

RUN python manage.py makemigrations

RUN python manage.py makemigrations debug attendance members status tasks password registration utilities

RUN python manage.py migrate

RUN python manage.py test

CMD ["bash", "/cms/start.sh"]